package org.cto.learn.html;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class HtmlformreceiverApplication {

	public static void main(String[] args) {
		SpringApplication.run(HtmlformreceiverApplication.class, args);
	}

	@Bean
	public ServletRegistrationBean exampleServletBean() {
		ServletRegistrationBean bean = new ServletRegistrationBean(
				new FormReceiverServlet(), "/htmlformReceiver/*");
		bean.setLoadOnStartup(1);
		return bean;
	}
}
