package org.cto.learn.html;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FormReceiverController {

    @PostMapping(path="receiveForm",consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<String> receiveFormData(@RequestParam MultiValueMap<String,String> paramMap){
        StringBuilder sb = new StringBuilder("<table>");
        paramMap.forEach((k,l) -> {
            final String keyValues  =  k + "\t" + l;
            sb.append("<tr><td>" + k + "</td><td>" + l + "</td></tr>");
            System.out.println(keyValues);
        });
        sb.append("</table>");
        return new ResponseEntity<String>(sb.toString(), HttpStatus.OK);
    }

}
